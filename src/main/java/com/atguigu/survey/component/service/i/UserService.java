package com.atguigu.survey.component.service.i;

import com.atguigu.survey.entities.guest.User;

public interface UserService{

    public abstract boolean regist(User user);

    public abstract boolean existsUserName(String userName);

}
