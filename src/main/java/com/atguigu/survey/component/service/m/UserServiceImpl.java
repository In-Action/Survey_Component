package com.atguigu.survey.component.service.m;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atguigu.survey.component.dao.i.UserMapper;
import com.atguigu.survey.component.service.i.UserService;
import com.atguigu.survey.entities.guest.User;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public boolean regist(User user) {
        int i = userMapper.insert(user);
        return i>0;
    }
    @Override
    public boolean existsUserName(String userName) {
        
        int count =  userMapper.isExistsUserName(userName);
        
        return count>0;
    }

}
